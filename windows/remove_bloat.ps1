# Remove bloat apps
$AppsList = "^*Microsoft.3DBuilder*",`
			"^*Microsoft.Messaging*",`
			"^*Microsoft.Office.OneNote*",`
			"^*Microsoft.ZuneMusic*",`
			"^*Microsoft.ZuneVideo*",`
			"^*Microsoft.WindowsPhone*",`
			"^*Microsoft.Office.Sway*",`
			"^*Microsoft.ConnectivityStore*",`
			"^*Microsoft.CommsPhone*",`
			"^*Microsoft.BingFinance*",`
			"^*Microsoft-WindowsFeedback*",`
			"^*RandomSaladGamesLLC*",`
			"^*Netflix*",`
			"^*McAfeeSecurity*",`
			"^*LinkedInforWindows*",`
			"^*EnergyStar*",`
			"^*HPJumpStarts*",`
			"^*HPSupportAssistant*",`
			"^*HPSystemEventUtility*",`
			"^*Amazon.com*",`
			"^*Microsoft.BingWeather*",`
			"^*Microsoft.Advertising.Xaml*",`
			"^*Microsoft.Advertising.Xaml*",`
			"^*Microsoft.Advertising.Xaml*",`
			"^*Microsoft.SkypeApp*",`
			"^*Microsoft.Windows.Cortana*",`
			"^*Microsoft.Windows.PeopleExperienceHost*",`
			"^*Microsoft.WindowsFeedbackHub*",`
			"^*PricelinePartnerNetwork.Booking.com*",`
			"^*Microsoft.GetHelp*",`
			"^*Microsoft.Getstarted*",`
			"^*Microsoft.Microsoft3DViewer*",`
			"^*Microsoft.MicrosoftOfficeHub*",`
			"^*Microsoft.MicrosoftSolitaireCollection*",`
			"^*Microsoft.OneConnect*",`
			"^*Microsoft.People*",`
			"^*Microsoft.Print3D*",`
			"^*Microsoft.Wallet*",`
			"^*microsoft.windowscommunicationsapps*",`
			"^*Microsoft.XboxGameOverlay*",`
			"^*Microsoft.XboxSpeechToTextOverlay*",`
			"^*Microsoft.Xbox.TCUI*",`
			"^*Microsoft.XboxApp*",`
			"^*Microsoft.XboxGameCallableUI*",`
			"^*Microsoft.XboxGamingOverlay*",`
			"^*Microsoft.XboxIdentityProvider*",`
			"^*Microsoft.XboxSpeechToTextOverlay*",`
			"^*Twitter*",`
			"^*Facebook*",`
			"^*ClearChannelRadioDigital.iHeartRadio*",`
			"^*Flipboard.Flipboard*",`
			"^*king.com.CandyCrushSaga*",`
			"^*king.com.CandyCrushSodaSaga*",`
			"^*Microsoft.3DBuilder*",`
			"^*Microsoft.BingFinance*",`
			"^*Microsoft.BingFoodAndDrink*",`
			"^*Microsoft.BingHealthAndFitness*",`
			"^*Microsoft.BingNews*",`
			"^*Microsoft.BingSports*",`
			"^*Microsoft.BingTranslator*",`
			"^*Microsoft.BingTravel*",`
			"^*Microsoft.BingWeather*",`
			"^*Microsoft.CommsPhone*",`
			"^*Microsoft.FreshPaint*",`
			"^*Microsoft.Getstarted*",`
			"^*Microsoft.MicrosoftJackpot*",`
			"^*Microsoft.MicrosoftJigsaw*",`
			"^*Microsoft.MicrosoftOfficeHub*",`
			"^*Microsoft.MicrosoftSolitaireCollection*",`
			"^*Microsoft.MicrosoftSudoku*",`
			"^*Microsoft.MinecraftUWP*",`
			"^*Microsoft.MovieMoments*",`
			"^*Microsoft.Office.OneNote*",`
			"^*Microsoft.Office.Sway*",`
			"^*Microsoft.People*",`
			"^*Microsoft.SkypeApp*",`
			"^*Microsoft.SkypeWiFi*",`
			"^*Microsoft.Studios.Wordament*",`
			"^*Microsoft.Taptiles*",`
			"^*microsoft.windowscommunicationsapps*",`
			"^*Microsoft.WindowsFeedback*",`
			"^*Microsoft.WindowsPhone*",`
			"^*Microsoft.XboxApp*",`
			"^*Microsoft.ZuneMusic*",`
			"^*Microsoft.ZuneVideo*",`
			"^*MicrosoftMahjong*",`
			"^*ShazamEntertainmentLtd.Shazam*",`
			"^*TheNewYorkTimes.NYTCrossword*"

ForEach ($app in $AppsList){
	$variable = DISM /Online /Get-ProvisionedAppxPackages | select-string Packagename
	$variable2 = $variable -replace "PackageName : ", ""
	
	$variable2 | % {DISM /Online /Remove-ProvisionedAppxPackage /PackageName:$_}
}

# Get list of apps installed
Get-AppxPackage -AllUsers | Select Name, PackageFullName | Sort Name


##Secure ciphersuite
$protocols = @{
  'SSL 2.0'= @{
    'Server-Enabled' = $false
    'Client-Enabled' = $false
  }
  'SSL 3.0'= @{
    'Server-Enabled' = $false
    'Client-Enabled' = $false
  }
  'TLS 1.0'= @{
    'Server-Enabled' = $false
    'Client-Enabled' = $true
  }
  'TLS 1.1'= @{
    'Server-Enabled' = $true
    'Client-Enabled' = $true
  }
  'TLS 1.2'= @{
    'Server-Enabled' = $true
    'Client-Enabled' = $true
  }
}


$protocols.Keys | ForEach-Object {
  
  Write-Output "Configuring '$_'"

  # create registry entries if they don't exist
  $rootPath = "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$_"
  if(-not (Test-Path $rootPath)) {
    New-Item $rootPath
  }

  $serverPath = "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$_\Server"
  if(-not (Test-Path $serverPath)) {
    New-Item $serverPath

    New-ItemProperty -Path $serverPath -Name 'Enabled' -Value 4294967295 -PropertyType 'DWord'
    New-ItemProperty -Path $serverPath -Name 'DisabledByDefault' -Value 0 -PropertyType 'DWord'
  }

  $clientPath = "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$_\Client"
  if(-not (Test-Path $clientPath)) {
    New-Item $clientPath
    
    New-ItemProperty -Path $clientPath -Name 'Enabled' -Value 4294967295 -PropertyType 'DWord'
    New-ItemProperty -Path $clientPath -Name 'DisabledByDefault' -Value 0 -PropertyType 'DWord'
  }
  
  # set server settings
  if($protocols[$_]['Server-Enabled']) {
    Set-ItemProperty -Path $serverPath -Name 'Enabled' -Value 4294967295
    Set-ItemProperty -Path $serverPath -Name 'DisabledByDefault' -Value 0
  } else {
    Set-ItemProperty -Path $serverPath -Name 'Enabled' -Value 0
    Set-ItemProperty -Path $serverPath -Name 'DisabledByDefault' -Value 1
  }
  
  # set client settings
  if($protocols[$_]['Client-Enabled']) {
    Set-ItemProperty -Path $clientPath -Name 'Enabled' -Value 4294967295
    Set-ItemProperty -Path $clientPath -Name 'DisabledByDefault' -Value 0
  } else {
    Set-ItemProperty -Path $clientPath -Name 'Enabled' -Value 0
    Set-ItemProperty -Path $clientPath -Name 'DisabledByDefault' -Value 1
  }
} 

# Make Transport Layer Security (TLS) 1.2 protocol registry keys
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client"

# Enable TLS 1.2 for client and server SCHANNEL communications

new-itemproperty -path     "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server" -name "Enabled" -value 1 -PropertyType "DWord"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server" -name "DisabledByDefault" -value 0 -PropertyType "DWord"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client" -name "Enabled" -value 1 -PropertyType "DWord"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client" -name "DisabledByDefault" -value 0 -PropertyType "DWord"

# Make and enable TLS 1.1 for client and server SCHANNEL communications

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Server"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Client"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Server" -name "Enabled" -value 1 -PropertyType "DWord"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Server" -name "DisabledByDefault" -value 0 -PropertyType "DWord"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Client" -name "Enabled" -value 1 -PropertyType "DWord"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Client" -name "DisabledByDefault" -value 0 -PropertyType "DWord"

# Disable Secure Sockets Layer (SSL) 2.0

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Server"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Server" -name Enabled -value 0 -PropertyType "DWord"

# Enable SSL 3.0
#md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Server"
#new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Server" -name Enabled -value 1 -PropertyType "DWord"

# Disable weak cyphers

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Null"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Null" -name "Enabled" -value 0 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\DES 56"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\DES 56/56"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\DES 56/56" -name "Enabled" -value 0 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 40"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 40/128"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 40/128" -name "Enabled" -value 0 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 56"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 56/128"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 40/128" -name "Enabled" -value 0 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 128"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 128/128"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 128/128" -name "Enabled" -value 0 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 40"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 40/128"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 40/128" -name "Enabled" -value 0 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 56"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 56/128"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 56/128" -name "Enabled" -value 0 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 64"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 64/128"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 64/128" -name "Enabled" -value 0 -PropertyType "Dword"

# Enable strong cyphers

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 128"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 128/128"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 128/128" -name "Enabled" -value 1 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Triple DES 168"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Triple DES 168/168"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Triple DES 168/168" -name "Enabled" -value 1 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\AES 128"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\AES 128/128"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\AES 128/128" -name "Enabled" -value 1 -PropertyType "Dword"

md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\AES 256"
md "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\AES 256/256"
new-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\AES 256/256" -name "Enabled" -value 1 -PropertyType "Dword"



##
## Windows 10 cleanup script.
## Remove dodgy tracking settings, unneeded services, all apps, and optional features that come with Windows 10. Make it more like Windows 7.
##
## Instructions
##  1. Start Windows 10 installation. At the first point you are asked for input during Windows 10 setup, hit Shift-F10. This gives you a command prompt.
##  2. Now mount a network volume, or insert a USB key with this script on it and change directory to it. e.g.
##     net use x: \\mynetworkmount
##     cd x:
##  3. Run the script
##     powershell -ExectionPolicy Bypass cleanup-win10.ps1
##  4. Let it run through, you may see a few errors, this is normal
##  5. Reboot
##     shutdown /r /t 0
##  6. Contine installation as normal
##  7. Once you login for the first time, run this script again (under Powershell as Administrator):
##     powershell -ExectionPolicy Bypass cleanup-win10.ps1
##  8. Go through the "Things to do after" shown at the end of the script
##  9. Done!
##

New-PSDrive -Name HKCR -PSProvider Registry -Root HKEY_CLASSES_ROOT

Write-Host 'Updating registry settings...'

# Disable some of the "new" features of Windows 10, such as forcibly installing apps you don't want, and the new annoying animation for first time login.
New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\' -Name 'CloudContent'
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name 'DisableWindowsConsumerFeatures' -PropertyType DWord -Value '1' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name 'DisableSoftLanding' -PropertyType DWord -Value '1' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System' -Name 'EnableFirstLogonAnimation' -PropertyType DWord -Value '0' -Force

# Set some commonly changed settings for the current user. The interesting one here is "NoTileApplicationNotification" which disables a bunch of start menu tiles.
New-Item -Path 'HKCU:\Software\Policies\Microsoft\Windows\CurrentVersion\' -Name 'PushNotifications'
New-ItemProperty -Path 'HKCU:\Software\Policies\Microsoft\Windows\CurrentVersion\PushNotifications' -Name 'NoTileApplicationNotification' -PropertyType DWord -Value '1' -Force
New-Item -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\' -Name 'CabinetState'
New-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\CabinetState' -Name 'FullPath' -PropertyType DWord -Value '1' -Force
New-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'HideFileExt' -PropertyType DWord -Value '0' -Force
New-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'Hidden' -PropertyType DWord -Value '1' -Force
New-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'ShowSyncProviderNotifications' -PropertyType DWord -Value '0' -Force

# Remove all Windows 10 apps, including Windows Store. You may not want this, but I don't ever use any of the apps or the start menu tiles.
# This makes Windows 10 similar to Windows 7. Don't forget to unpin all the tiles after installation to trim down the start menu!
Get-AppxProvisionedPackage -Online | Remove-AppxProvisionedPackage -Online
Get-AppxPackage | Remove-AppxPackage

# Disable Cortana, and disable any kind of web search or location settings.
New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\' -Name 'Windows Search'
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name 'AllowCortana' -PropertyType DWord -Value '0' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name 'AllowSearchToUseLocation' -PropertyType DWord -Value '0' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name 'DisableWebSearch' -PropertyType DWord -Value '1' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name 'ConnectedSearchUseWeb' -PropertyType DWord -Value '0' -Force

# Remove OneDrive, and stop it from showing in Explorer side menu.
C:\Windows\SysWOW64\OneDriveSetup.exe /uninstall
Remove-Item -Path 'HKCR:\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}' -Recurse
Remove-Item -Path 'HKCR:\Wow6432Node\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}' -Recurse

# Disable data collection and telemetry settings.
New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer' -Name 'SmartScreenEnabled' -PropertyType String -Value 'Off' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection' -Name 'AllowTelemetry' -PropertyType DWord -Value '0' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection' -Name 'AllowTelemetry' -PropertyType DWord -Value '0' -Force

# Disable Windows Defender submission of samples and reporting.
New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\' -Name 'Spynet'
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet' -Name 'SpynetReporting' -PropertyType DWord -Value '0' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet' -Name 'SubmitSamplesConsent' -PropertyType DWord -Value '2' -Force

# Ensure updates are downloaded from Microsoft instead of other computers on the internet.
New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\' -Name 'DeliveryOptimization'
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\DeliveryOptimization' -Name 'DODownloadMode' -PropertyType DWord -Value '0' -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\DeliveryOptimization' -Name 'SystemSettingsDownloadMode' -PropertyType DWord -Value '0' -Force
New-Item -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\' -Name 'Config'
New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config' -Name 'DODownloadMode' -PropertyType DWord -Value '0' -Force

Write-Host 'Disabling services...'
$services = @(
    # See https://virtualfeller.com/2017/04/25/optimize-vdi-windows-10-services-original-anniversary-and-creator-updates/

    # CDPSvc doesn't seem to do anything useful, that I found. See note on CDPUserSvc further down the script
    'CDPSvc',

    # Connected User Experiences and Telemetry
    'DiagTrack',

    # Data Usage service
    'DusmSvc',

    # Peer-to-peer updates
    'DoSvc',

    # AllJoyn Router Service (IoT)
    'AJRouter',

    # SSDP Discovery (UPnP)
    'SSDPSRV',
    'upnphost',

    # Superfetch
    'SysMain',

    # http://www.csoonline.com/article/3106076/data-protection/disable-wpad-now-or-have-your-accounts-and-private-data-compromised.html
    'iphlpsvc',
    'WinHttpAutoProxySvc',

    # Black Viper 'Safe for DESKTOP' services.
    # See http://www.blackviper.com/service-configurations/black-vipers-windows-10-service-configurations/
    'tzautoupdate',
    'AppVClient',
    'RemoteRegistry',
    'RemoteAccess',
    'shpamsvc',
    'SCardSvr',
    'UevAgentService',
    'ALG',
    'PeerDistSvc',
    'NfsClnt',
    'dmwappushservice',
    'MapsBroker',
    'lfsvc',
    'HvHost',
    'vmickvpexchange',
    'vmicguestinterface',
    'vmicshutdown',
    'vmicheartbeat',
    'vmicvmsession',
    'vmicrdv',
    'vmictimesync',
    'vmicvss',
    'irmon',
    'SharedAccess',
    'MSiSCSI',
    'SmsRouter',
    'CscService',
    'SEMgrSvc',
    'PhoneSvc',
    'RpcLocator',
    'RetailDemo',
    'SensorDataService',
    'SensrSvc',
    'SensorService',
    'ScDeviceEnum',
    'SCPolicySvc',
    'SNMPTRAP',
    'TabletInputService',
    'WFDSConSvc',
    'FrameServer',
    'wisvc',
    'icssvc',
    'WinRM',
    'WwanSvc',
    'XblAuthManager',
    'XblGameSave',
    'XboxNetApiSvc'
)
foreach ($service in $services) {
    Set-Service $service -StartupType Disabled
}

# CDPUserSvc is a mysterious service that just seems to throws errors in the event viewer. I haven't seen any problems with it disabled.
# See https://social.technet.microsoft.com/Forums/en-US/c165a54a-4a69-441c-94a7-b5712b54385d/what-is-the-cdpusersvc-for-?forum=win10itprogeneral
# Note that the related service CDPSvc is also disabled in the above services loop. CDPUserSvc can't be disabled by Set-Service, due to a random
# hash after the service name, but disabling via the registry is perfectly fine.
Write-Host 'Disabling CDPUserSvc...'
Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Services\CDPUserSvc' -Name 'Start' -Value '4'

Write-Host 'Disabling hibernate...'
powercfg -h off

# Disables all of the known enabled-by-default optional features. There are some particulary bad defaults like SMB1. Sigh.
Write-Host 'Disabling optional features...'
$features = @(
    'MediaPlayback',
    'SMB1Protocol',
    'Xps-Foundation-Xps-Viewer',
    'WorkFolders-Client',
    'WCF-Services45',
    'NetFx4-AdvSrvs',
    'Printing-Foundation-Features',
    'Printing-PrintToPDFServices-Features',
    'Printing-XPSServices-Features',
    'MSRDC-Infrastructure',
    'MicrosoftWindowsPowerShellV2Root',
    'Internet-Explorer-Optional-amd64'
)
foreach ($feature in $features) {
    Disable-WindowsOptionalFeature -Online -FeatureName $feature -NoRestart
}

sc stop BITS & sc config BITS start= disabled

Write-Host 'Finished. Reboot system with: shutdown /r /t 0'
Write-Host ''
Write-Host '***'
Write-Host 'If you ran this script during installation, make sure to run it again after logging in for the first time!'
Write-Host '***'
Write-Host ''
Write-Host '***'
Write-Host 'Things to do after you have logged in for the first time and ran the script again:'
Write-Host ' * Unpin all the tiles in the start menu'
Write-Host ' * Settings > Privacy: switch off all the radio buttons through all menus on the left'
Write-Host ' * Settings > Privacy > Feedback & diagnostics menu: select Never for Feedback frequency'
Write-Host ' * Settings > Apps, click the Manage optional features link. Remove Contact Support, Internet Explorer 11, Microsoft Quick Assist, and Windows Media Player'
Write-Host ' * Settings > System > Notifications & actions: switch off "Get notifications from apps and other senders"'
Write-Host ' * Settings > System > Notifications & actions: switch off "Show me the Windows welcome experience..."'
Write-Host ' * Settings > System > Shared experiences: switch off "Let me open apps on other devices..."'
Write-Host ' * Settings > Update & security, click the Advanced options link. Tick "Give me updates for other Microsoft products when I update Windows."'
Write-Host ' * Run Windows Update for the first time!'
Write-Host '***'
