[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11"
foreach($line in Get-Content ./download_links.txt) {
	if($line -match $regex) {
		Try  {
			#Work here
			echo "Downloading file: " $line
			$url = $line
			$req = [System.Net.HttpWebRequest]::Create($url)
			$req.Method = "HEAD"
			$response = $req.GetResponse()
			$fUri = $response.ResponseUri
			$filename = [System.IO.Path]::GetFileName($fUri.LocalPath);
			$response.Close()

			(New-Object System.Net.WebClient).DownloadFile($url,"C:\Users\"+$env:userName+"\Downloads\" + $filename)
		}
		Catch 
		{
			$ErrorMessage = $_.Exception.Message
			$FailedItem = $_.Exception.ItemName
			echo $ErrorMessage $FailedItem
		}
	}
}

#Disable SSL
#[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

Read-Host -Prompt "Press Enter to exit"
