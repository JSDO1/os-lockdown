import os
import sys
import hashlib
import subprocess as sp
import re
import json

ENC = sys.getdefaultencoding()
ERR = 'replace'

apk_list = ["com.instagram.android", "com.snapchat.android", "com.facebook.katana"]
TOKENFILE=os.path.expanduser('~/.cache/gplaycli/token')

#RE_APPEND_VERSION=re.compile("^"+apk.replace('.', r'\.')+r"-v.[A-z0-9.-]+\.apk$")

def call(args):
	proc = sp.run(args.split(), stdout=sp.PIPE, stderr=sp.PIPE)
	print(proc.stdout.decode(ENC, ERR), file=sys.stdout)
	print(proc.stderr.decode(ENC, ERR), file=sys.stderr)
	return proc

def nblines(comp_proc):
	return len(comp_proc.stdout.decode(ENC, ERR).splitlines(True))

def download_apk(apk, dc='hammerhead', append_version = False):
	if append_version:
		call("gplaycli -av -L -vd %s -dc %s" % (apk, dc))
	else:
		call("gplaycli -L -vd %s -dc %s" % (apk, dc))

def checksum(apk):
	return hashlib.sha256(open(apk, 'rb').read()).hexdigest()

def start_download(apk):
    while True:
        if os.path.isfile(TOKENFILE):
            os.remove(TOKENFILE)
        download_apk(apk, 'hammerhead', False)
        if os.path.isfile("%s.apk" % apk) == False:
            print('Download failed')
        else:
            break
   
def download_apk_list(apk_list):
    for apk in apk_list:
        print('Starting download for %s\n' % apk)
        start_download(apk)
            
download_apk_list(apk_list)